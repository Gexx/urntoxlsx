var winston = require('winston');

var myCustomLevels = {
	levels: {
		debug: 0,
		info: 1,
		data: 1,
		error: 2
	},
	colors: {
		debug: 'magenta',
		info: 'yellow',
		data: 'blue',
		error: 'red'
	}
};

var logger = new (winston.Logger)({
	levels: myCustomLevels.levels,
	colors: myCustomLevels.colors,
	transports: [
		new (winston.transports.Console)({
			colorize: true,
			level: 'error'
		}),
		new (winston.transports.File)({
			name: 'error-file',
			filename: '.logs/converter.log',
			level: 'error'
		})
	]
});

module.exports = logger;