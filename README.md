# UltiCAD URN to XLSX converter

### Description
This is program that converts UltiCAD URN file to XLSX file, coded by Gordan Nekić.

### Installation
You need to run:
```sh
$ npm install
```

Put all files to convert into folder 
```sh
/ULAZ/*.urn
```

Ok, now you are ready to go! Run!
```sh
$ node app.js
```

### Todos

 - Add more Code Comments
 - Fix bug where unexpectedly sometimes some function return "Cthulhu" ;)
