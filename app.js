// Require tools
var fs		= require('fs');
var path	= require('path');
var ARGS	= require('shell-arguments');
var figlet	= require('figlet'); // Just for fun, ASCII art FTW! ;)
var colors	= require('colors');

var parseString = require('xml2js').parseString;
var xlsx		= require('node-xlsx');
 
 // Asii art
console.log(figlet.textSync('UltiCAD', {font: 'Small'}).yellow);
console.log(figlet.textSync('URN to XLSX', {font: 'Small'}).yellow);
console.log();
console.log(' ############################################################################## '.red);
console.log('  Ovaj program zasticen je autorskim pravom autora, za dobivanje licence '.yellow);
console.log('  kontaktirajte me na email: nekic.gordan@gmail.com '.yellow);
console.log('  Verzija programa 0.0.3 | KLEK d.o.o.'.yellow);
console.log('  by Gordan Nekić '.yellow);
console.log(' ############################################################################## '.red);

// Loading logger
var logger = require('./packets/logger.js');


// Item codes
var itemCodeList = {};

// Excluded list!
var excludedItems_ORMAR = [];
var excludedItems_KUHINJA = [];

// Rename lista
var renameList = [];

// Load lists
fs.readFile('./LIST/code_list.json', function(err, data){
	if (err){ logger.error(err); return; }
	
	logger.info('Loading code list...');
	itemCodeList = JSON.parse(data);

	// Reading exclusion list
	fs.readFile('./LIST/exclusion_list_ormar.json', function(err, data){
		if (err){ logger.error(err); return; }
		
		logger.info('Loading excluded ORMAR list...');
		excludedItems_ORMAR = JSON.parse(data);

		// Reading exclusion list
		fs.readFile('./LIST/exclusion_list_kuhinja.json', function(err, data){
			if (err){ logger.error(err); return; }
			
			logger.info('Loading excluded KUHINJA list...');
			excludedItems_KUHINJA = JSON.parse(data);

			// Reading rename list
			fs.readFile('./LIST/rename_list.json', function(err, data){
				if (err){ logger.error(err); return; }
				
				logger.info('Loading renaming list...');
				renameList = JSON.parse(data);

				LOAD_FILES();
			});

		});
	});
});

function LOAD_FILES(){
	var p = './ULAZ/';
	fs.readdir(p, function (err, files) {
		if(err){logger.error(err); return;}

		files.map(function (file) {
			return path.join(p, file);
		}).filter(function (file) {
			return fs.statSync(file).isFile();
		}).forEach(function (file) {
			if(path.extname(file) === '.urn'){
				//console.log("%s (%s) (%s)", file, path.extname(file), path.basename(file, path.extname(file)));
				var base_name = path.basename(file, path.extname(file));

				fs.readFile(file, function(err, data){
					if (err){ logger.error(err); return; }

					var xml = data;
					GenerateXLSX(xml, path.dirname(file)+'/'+base_name+' - Radna Lista - ORMAR.xlsx', excludedItems_ORMAR, renameList );
					GenerateXLSX(xml, path.dirname(file)+'/'+base_name+' - Radna Lista - KUHINJA.xlsx', excludedItems_KUHINJA, renameList );
				});
			}
		});
	});
}

function ClearCroLetters(str){
	return str.replace('č', 'c').replace('ć', 'c').replace('ž', 'z').replace('š', 's').replace('đ', 'd').replace('Č', 'C').replace('Ć', 'C').replace('Ž', 'Z').replace('Š', 'S').replace('Đ', 'D');
}

function GenerateXLSX(xml, output, excludedItems, renameList){

	var xlsx_data = [
		['R. br.', 'Šifra', 'Naziv Artikla', 'Količina', 'J. mj.']
	];

	parseString(xml, function (err, result) {
		//console.dir(result);
		//console.log(result['KuhDescriptor']['ItemList'][0]['Item']);
		//console.log(result['KuhDescriptor']['PanelItemList'][0]['PanelItem']);

		var ItemList = result['KuhDescriptor']['ItemList'][0]['Item'];
		var PanelItemList = result['KuhDescriptor']['PanelItemList'][0]['PanelItem'];


		var itemElementsArray = [];
		var itemArray = [];
		var itemOUTPUT = [];

		var itemNum = 0;

		var tempItem;
		var itemName;
		var itemCode;
		var itemJdmj;
		for(var i = 0; i < ItemList.length; i++){
			tempItem = ItemList[i];

			// If it exist go to next iteration.
			if(excludedItems.indexOf(tempItem['Item'][0]['Name'][0].toString()) !== -1){
				continue;
			}

			// If new element is detected, it does not exist in array yet.
			if(itemElementsArray.indexOf(tempItem['Element'][0].toString()) === -1){
				//logger.info('--- NEW ELEMENT ---');
				itemElementsArray.push(tempItem['Element'][0].toString());
			}

			itemNum++;

			// If new item is detected, it does not exist in array yet.
			if(itemArray.indexOf(tempItem['Item'][0]['Name'][0].toString()) === -1){
				//logger.info('--- NEW ITEM ---');
				itemArray.push(tempItem['Item'][0]['Name'][0]);
				itemOUTPUT.push(tempItem);
			}else{
				// Add quantity!
				var x;
				for(x = 0; x < itemOUTPUT.length; x++){
					if( itemOUTPUT[x]['Item'][0]['Name'][0].toString() === tempItem['Item'][0]['Name'][0].toString()){
						//console.log('FOUND x! ', x);
						break;
					}
				}
				//console.log('Before adding !', itemOUTPUT[x]['Quantity'][0], tempItem['Quantity'][0]);
				itemOUTPUT[x]['Quantity'][0] = parseFloat(itemOUTPUT[x]['Quantity'][0]) + parseFloat(tempItem['Quantity'][0]);
				//console.log('After adding !', itemOUTPUT[x]['Quantity'][0]);
			}
		}

		for(i = 0; i < itemOUTPUT.length; i++){
			tempItem = itemOUTPUT[i];

			// Check for code in codelist
			if(typeof itemCodeList[ tempItem['Item'][0]['Name'][0] ] === 'undefined'){
				itemCode = '';
			}else{
				itemCode = itemCodeList[ tempItem['Item'][0]['Name'][0] ];
			}

			// Check for rename list
			if(typeof renameList[ tempItem['Item'][0]['Name'][0] ] === 'undefined'){
				itemName = tempItem['Item'][0]['Name'][0];
			}else{
				itemName = renameList[ tempItem['Item'][0]['Name'][0] ];
			}

			// Change jd. mjeru
			if(tempItem['ItemType'][0] === '2'){
				itemJdmj = 'm';
			}else{
				itemJdmj = 'kol';
			}

			// Add to the output
			logger.info((i+1)+'# | '+tempItem['Element'][0]+' | '+itemCode+' | '+tempItem['Item'][0]['Name'][0]+' --> '+itemName+' | '+tempItem['Quantity'][0]+' | '+itemJdmj);
			//xlsx_data.push([(i+1), tempItem['Element'][0], itemCode, itemName, '=NUMBERVALUE("'+tempItem['Quantity'][0]+'", ".")', itemJdmj]);
			xlsx_data.push([(i+1), itemCode, itemName, parseFloat(tempItem['Quantity'][0]).toFixed(3), itemJdmj]);
		}

		var buffer = xlsx.build([{name: "Stranica1", data: xlsx_data}]); // returns a buffer 
		fs.writeFile(output, buffer, function (err,data) {
			if (err) { return console.log(err); }
		});
	});

}